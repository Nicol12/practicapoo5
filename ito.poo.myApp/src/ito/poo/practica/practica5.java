package ito.poo.practica;
  import java.time.LocalDate;
public class practica5 {
	
	private LocalDate CosechaxTiempo;
	private LocalDate TiempoCosecha;
	private LocalDate CantCosechaxTiempo;
	private int Costoxperiodo;
	private int Gananciaxperiodo;
	
	public practica5(LocalDate cosechaxtiempo, LocalDate tiempocosecha, LocalDate cantcosechaxtiempo, int costoxperiodo, int gananciaxperiodo) {
		super();
		CosechaxTiempo = cosechaxtiempo;
		TiempoCosecha = tiempocosecha;
		CantCosechaxTiempo = cantcosechaxtiempo;
		Costoxperiodo = costoxperiodo;
		Gananciaxperiodo = gananciaxperiodo;
	}

	public practica5(String string) {
		// TODO Auto-generated constructor stub
	}

	public LocalDate getCantCosechaxTiempo() {
		return CantCosechaxTiempo;
	}

	public void setCantCosechaxTiempo(LocalDate cantCosechaxTiempo) {
		CantCosechaxTiempo = cantCosechaxTiempo;
	}

	public LocalDate getCosechaxTiempo() {
		return CosechaxTiempo;
	}

	public LocalDate getTiempoCosecha() {
		return TiempoCosecha;
	}

	public int getCostoxperiodo() {
		return Costoxperiodo;
	}

	public int getGananciaxperiodo() {
		return Gananciaxperiodo;
	}

	@Override
	public String toString() {
		return "practica5 [CosechaxTiempo=" + CosechaxTiempo + ", TiempoCosecha=" + TiempoCosecha
				+ ", CantCosechaxTiempo=" + CantCosechaxTiempo + ", Costoxperiodo=" + Costoxperiodo
				+ ", Gananciaxperiodo=" + Gananciaxperiodo + ", toString()=" + super.toString() + "]";
	}
	
				
}

